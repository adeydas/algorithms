/*
 * Partition problem is to determine whether a given set can be partitioned into two subsets such that the sum of elements in both subsets is same.

Examples

arr[] = {1, 5, 11, 5}
Output: true 
The array can be partitioned as {1, 5, 5} and {11}

arr[] = {1, 5, 3}
Output: false 
The array cannot be partitioned into equal sum sets.

Following are the two main steps to solve this problem:
1) Calculate sum of the array. If sum is odd, there can not be two subsets with equal sum, so return false.
2) If sum of array elements is even, calculate sum/2 and find a subset of array with sum equal to sum/2. 
 */
package ws.abhis.Dynamic;

public class PartitionProblem {
	
	/*
	 * part[i][j] = true if a subset of {arr[0], arr[1], ..arr[j-1]} has sum 
             equal to i, otherwise false
	 */
	public boolean findPartition(int[] arr, int n) {
		int sum = 0;
		int i, j;
		
		for(i=0; i<n; i++) 
			sum += arr[i];
		
		//if sum is odd then such a division is not possible
		if(sum %2 != 0)
			return false;
		
		//initialize 2D array for [sum/2+1][n+1]
		boolean part[][] = new boolean[sum/2+1][n+1];
		
		//initialize top row as true
		for(i=0; i<=n; i++) {
			part[i][0] = true;
		}
		
		return false;
	}
}
