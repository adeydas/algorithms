/*
 * Given Two separate integer arrays,find the combined median of the two arrays without using any extra array
 */

package ws.abhis.quicksort;

public class MicrosoftQ1 {
	
	
	public int sort(int[] arr1, int[] arr2) {
		int totalLength = (arr1.length + arr2.length);
		int medianL = totalLength/2;
		QuickSort1 q1 = new QuickSort1();
		QuickSort1 q2 = new QuickSort1();
		q1.sort(arr1);
		q2.sort(arr2);
		if(medianL > q1.numbers.length) {
			//on other side
			return q2.numbers[medianL];
		} else {
			//on this side
			return q1.numbers[medianL];
		}
	}
	
	public static void main(String args[]) {
		int[] arr1 = {10,9,6,1,12};
		int[] arr2 = {4,5,2,1};
		
		MicrosoftQ1 obj = new MicrosoftQ1();
		System.out.println(obj.sort(arr1, arr2));
	}
}
