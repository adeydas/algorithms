package ws.abhis.quicksort;

public class QuickSort1 {
	
	public int[] numbers;
	private int number;
	
	public void sort(int[] values) {
		if(values == null || values.length == 0)
			return;
		this.numbers = values;
		number = values.length;
		quicksort(0, number-1);
	}
	
	private void quicksort(int low, int high) {
		int i=low, j=high;
		//good way for odd ranges
		int pivot = numbers[low + (high-low)/2];
		
		//divide into two lists
		while(i<=j) {
			while(numbers[i] < pivot)
				i++;
			
			//>= made to > to avoid index out of bounds exception
			while(numbers[j] > pivot)
				j--;
			
			if(i<=j) {
				exchange(i,j);
				i++; j--;
			}
		}
		
		if(low<j)
			quicksort(low, j);
		if(i<high)
			quicksort(i, high);
	}

	private void exchange(int i, int j) {
		 int temp = numbers[i];
		 numbers[i] = numbers[j];
		 numbers[j] = temp;
		
	}
	
	//run
	public static void  main(String args[]) {
		System.out.println("Inplace quick sort....");
		int[] test = {100, 200, 5, 4, 33};
		QuickSort1 q = new QuickSort1();
		q.sort(test);
		for(int i=0; i<q.numbers.length; i++) {
			System.out.println(q.numbers[i]+"\t");
		}
	}
}
