/*
 * A) Write a function that takes input as integer and converts it into a linked list where each node represents a digit of the input integer.

e.g. i/p : 123
expected result: [1] => [2] => [3] => [null]
(catch: don't forget to consider case of negatives like -433 etc.)

B) Write a function that takes input as two linked lists shown as above (which are basically integers represented in linked list format) and calculate sum.
 */

package ws.abhis.LinkedList;

public class NumbersToLinkedList {
	public class Digit {
		public int getValue() {
			return value;
		}
		public void setValue(int value) {
			this.value = value;
		}
		public Digit getNode() {
			return node;
		}
		public void setNode(Digit node) {
			this.node = node;
		}
		private int value;
		private Digit node;
	}
	
	public void createLinkedList(int number, Digit d) {
		
		int digit = number %10;
		number = number/10;
		
		Digit d1 = new Digit();
		d1.setValue(digit);
		d1.setNode(null);
		d.setNode(d1);
		
		createLinkedList(number, d1);
		
	}

}
