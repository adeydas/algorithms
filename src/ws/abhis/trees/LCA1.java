/*
 * Find the LCA of two given nodes in a binary tree if the nodes contain pointer to its parent.
 */
package ws.abhis.trees;

public class LCA1 {
	
	
	public Node n1;
	public Node n2;
	
	//normalize the nodes to the same level
	private void preProceess() {
		int leveln1 = 0, leveln2=0;
		Node n1temp = n1;
		Node n2temp = n2;
		while(n1temp != null) {
			leveln1++;
			n1temp = n1temp.parent;
		}
		while(n2temp != null) {
			leveln2++;
			n2temp = n2temp.parent;
		}
		
		
		while(leveln1 != leveln2) {
			if(leveln1 > leveln2) {
				n1 = n1.parent;
				leveln1--;
			} else {
				n2 = n2.parent;
				leveln2--;
			}
		}
	}
	
	//traverse bottom up and find the LCA
	private Node traverse() {
		while (n1!=null) {
			if(n1 == n2)
				return n1;
			else {
				n1 = n1.parent;
				n2 = n2.parent;
			}
		}
		return null;
	}
	
	public static void main(String[] args) {
		LCA1 obj = new LCA1();
		Node n12 = new Node(12);
		Node n11 = new Node(11);
		Node n14 = new Node(14);
		Node n15 = new Node(15);
		Node n7 = new Node(7);
		Node n2 = new Node(2);
		Node n5 = new Node(5);
		Node n9 = new Node(9);
		
		//form the tree
		n12.parent = n14;
		n14.parent = n7;
		n15.parent = n7;
		n11.parent = n5;
		n9.parent = n5;
		n5.parent = n2;
		n7.parent = n2;
		n2.parent = null;
		
		Node nFirst = n11;
		Node nSecond = n9;
		obj.n1 = nFirst;
		obj.n2 = nSecond;
		obj.preProceess();
		Node ret = obj.traverse();
		
		//print LCA
		System.out.println(ret.value);
	}
	
}


