/*
 * Given array of words, group the anagrams
IP:{tar,rat,banana,atr}
OP:{[tar,rat,atr],[banana]}
 */

package ws.abhis.misc;

public class Anagrams {
	public static int NO_OF_CHARS = 26;
	
	class IndexNode {
		int index;
		IndexNode next;
	}
	
	class TrieNode {
		boolean isEnd;
		TrieNode child[];
		IndexNode head;
	}
	
	TrieNode createNewTrie() {
		TrieNode temp = new TrieNode();
		temp.isEnd = false;
		temp.head = null;
		for(int i=0; i< NO_OF_CHARS; ++i) {
			temp.child[i] = null;
		}
		return temp;
	}
}
