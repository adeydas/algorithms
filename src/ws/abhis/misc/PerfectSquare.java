/*
 * Write a program to find whether a given number is a perfect square or not. You can only use addition and subtraction operation to find a solution with min. complexity.

 */
package ws.abhis.misc;

public class PerfectSquare {
	public boolean isPerfectSquare(int number) {
		int sum = 0;
		
		for(int i=1; i<number; i=i+2) {
			sum += i;
			if(sum == number)
				return true;
		}
		return false;
	}
	
	public static void main(String args[]) {
		PerfectSquare pq = new PerfectSquare();
		if(pq.isPerfectSquare(5))
			System.out.println("Yes");
		else 
			System.out.println("No");
	}
}
