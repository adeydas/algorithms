/*
 * Given a float number 7.64, convert it into the string WITHOUT using any inbuilt function/library.
 */

package ws.abhis.misc;

public class ConvertToString {
	
	public String convert(float f) {
		int beforeDecimal = (int)f;
		int afterDecimal = (int) ((float)(f - beforeDecimal) * 100);
		String r = concatString(beforeDecimal) + "." + concatString(afterDecimal);
		return r;
	}
	
	private String concatString(int n) {
		String ret = "";
		while(n!=0) {
			int k = n%10;
			n = n/10;
			ret += k;
		}
		ret = new StringBuilder(ret).reverse().toString();
		return ret;
	}
	
	public static void main(String[] args) {
		ConvertToString obj = new ConvertToString();
		System.out.println(obj.convert(7.64f));
	}
	
}
