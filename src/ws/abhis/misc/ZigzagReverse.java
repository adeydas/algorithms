/*
 * Write a program to reverse a sentence in a zigzag order.
i/p: I am a software programmer
o/p: programmer erawtfos a ma I
 */
package ws.abhis.misc;

public class ZigzagReverse {
	public void zigzag(String word) {
		String[] words = word.split(" ");
		
		for(int i=words.length-1; i>= 0; i--) {
			if(i%2 != 0) {
				char[] w = words[i].toCharArray();
				for(int j=w.length-1; j>=0; j--) {
					System.out.print(w[j]);
				}
				System.out.print(" ");
			} else {
				System.out.print(words[i]+" ");
			}
		}
	}
	
	public static void main(String args[]) {
		ZigzagReverse obj = new ZigzagReverse();
		obj.zigzag("I am a software programmer");
	}
}
