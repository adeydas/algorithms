/*
 * Given two arrays find the intersection of those two arrays. Besides using hash table can we attain the same time complexity that is O(m+n) by using some other approach.
 */

//for sorted array

package ws.abhis.misc;

public class ArrayIntersection {
	public void findIntersection(int[] arr1, int[] arr2) {
		int length1 = arr1.length, length2 = arr2.length;
		int i=0, j=0;
		while (i<length1 && j<length2) {
			if(arr1[i] == arr2[j]) {
				System.out.println(arr1[i]+"\t");
				i++; j++;
			}
			else if (arr1[i] > arr2[j]) {
				j++;
			} else if (arr1[i] < arr2[j]) {
				i++;
			} else if (arr1[i] == arr2[j]) {
				i++; j++;
			}
		}
	}
	
	public static void main (String args[]) {
		int arr1[] = {1,2,3,4,5};
		int arr2[] = {3,4,5,6,7,8};
		
		ArrayIntersection obj = new ArrayIntersection();
		obj.findIntersection(arr1, arr2);
		
	}
	
	
}
