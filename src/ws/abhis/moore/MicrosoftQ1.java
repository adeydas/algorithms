/*
 * Find the majority element which occurs more than n/2 times in an array of n size, which contains duplicate elements in minimum time and space complexity.
 */

package ws.abhis.moore;

public class MicrosoftQ1 {
	
	public void findElement(int[] elements) {
		Moore m = new Moore();
		int candidate = m.findMajority(elements);
		int count = 0;
		for(int i=0; i<elements.length; i++) {
			if(elements[i] == candidate)
				count++;
		}
		
		if(count > (elements.length/2)) {
			System.out.println("Element "+candidate);
		} else {
			System.out.println("No such element");
		}
		
	}
	
	public static void main(String args[]) {
		int[] candidates = {1,2,3,4,2,4,4,4,4,4};
		MicrosoftQ1 obj = new MicrosoftQ1();
		obj.findElement(candidates);
	}
	
}
