//find majority in single sweep
package ws.abhis.moore;

public class Moore {
	
	//the array candidates and votes should be of the same size where
	//the array candidate would contain the candidate id and votes
	//the number of votes that particular candidate got
	public int findMajority (int[] candidates) {
		int c=0, count=0;
		
		for(int i=0; i<candidates.length; i++) {
			if(count == 0) {
				c = candidates[i];
				count++;
			} else {
				if(c != candidates[i]) {
					count--;
				} else {
					count++;
				}
			}
		}
		
		System.out.println("Candidate: "+c);
		return c;
	}
	
	public static void main(String args[]) {
		int[] candidates = {1,2,3,4,2,1,4,4,4,2};
		Moore m = new Moore();
		m.findMajority(candidates);
	}
	
}
